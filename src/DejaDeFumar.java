public class DejaDeFumar {
    private static Thread t1, t2;

    public static void main(String[] args) {
        Runnable r1 = DejaDeFumar::fumador;
        t1 = new Thread(r1);
        t1.start();

        Runnable r2 = DejaDeFumar::medico;
        t2 = new Thread(r2);
        t2.start();
    }

    private static void medico() {
        try {
            Thread.sleep(3000);
            t1.interrupt();
        } catch (InterruptedException e) {
            System.out.println("Algo salio mal...");
        }
    }

    private static void fumador() {
        int i = 0;
        try {
            while (true) {
                i++;
                System.out.println("Estoy fumando... " + i);
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}